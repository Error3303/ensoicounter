package com.error3303.ensoicounter;

import android.app.Application;

import com.error3303.ensoicounter.database.EnSoiDao;
import com.error3303.ensoicounter.database.EnSoiDatabase;

import lombok.Getter;

public class MyApplication extends Application {

    @Getter
    private volatile EnSoiDao dao;

    private EnSoiDatabase database;

    /**
     * Lors du démarrage de l'application recupération de la dao.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        database = EnSoiDatabase.getInstance(getApplicationContext());
        dao = database.enSoiDao();
    }

    /**
     * Permet la fermeture de la base de données
     */
    public void closeDatabase() {
        if (database != null) {
            database.getOpenHelper().close();
        }
    }
}
