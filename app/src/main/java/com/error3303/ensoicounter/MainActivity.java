package com.error3303.ensoicounter;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.error3303.ensoicounter.database.IServicePersistance;
import com.error3303.ensoicounter.database.ServicePersistance;
import com.error3303.ensoicounter.entity.EnSoi;
import com.error3303.ensoicounter.stats.EnSoiStatsDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.lbl_compteur)
    TextView lblCompteur;
    @BindView(R.id.btn_plus)
    Button btnPlus;
    @BindView(R.id.btn_moins)
    Button btnMoins;
    @BindView(R.id.btn_raz)
    Button btnRAZ;
    @BindView(R.id.btn_stats)
    Button btnStats;

    private int compteur;
    private IServicePersistance servicePersistance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        servicePersistance = new ServicePersistance(this.getApplication());
        init();
    }

    /**
     * Initialisation de l'app
     */
    private void init() {
        compteur = 0;
        observeLiveData();
    }

    /**
     * Ajoute un EnSoi à la bdd.
     */
    @OnClick(R.id.btn_plus)
    public void increment() {
        servicePersistance.insert(new EnSoi());
    }

    /**
     * Supprime le dernier EnSoi de la bdd.
     */
    @OnClick(R.id.btn_moins)
    public void decrement() {
        servicePersistance.deleteLastEnSoi();
    }

    /**
     * Supprime l'ensemble des EnSoi de la bdd.
     */
    @OnClick(R.id.btn_raz)
    public void reset() {
        servicePersistance.deleteAll();
        compteur = 0;
    }

    /**
     * Mise à jour des éléments (visibilité des boutons, valeur du compteur)
     */
    private void updateElements() {
        showHideButtons();
        updateLabelCompteur();
    }

    /**
     * Méthode gérant l'affichage ou non des boutons - et RAZ.
     */
    private void showHideButtons() {
        if (compteur == 0) {
            hideButtons();
        } else {
            showButtons();
        }
    }

    /**
     * Mets à jour le label du nombre d'occurences.
     */
    private void updateLabelCompteur() {
        lblCompteur.setText(String.valueOf(compteur));
    }

    /**
     * Cache les boutons - et RAZ.
     */
    private void hideButtons() {
        btnMoins.setVisibility(View.INVISIBLE);
        btnRAZ.setVisibility(View.INVISIBLE);
    }

    /**
     * Affiche les boutons - et RAZ.
     */
    private void showButtons() {
        btnMoins.setVisibility(View.VISIBLE);
        btnRAZ.setVisibility(View.VISIBLE);
    }

    /**
     * Méthode ajoutant un observer à la liste des occurences de la table ensoi.
     */
    private void observeLiveData() {
        servicePersistance.getEnsois().observe(this, enSois -> {
            compteur = enSois.size();
            updateElements();
        });
    }

    /**
     * Créé et ouvre un DialogFragment pour contenant le layout des stats.
     */
    @OnClick(R.id.btn_stats)
    public void openStatsActivity(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        EnSoiStatsDialogFragment editNameDialogFragment = EnSoiStatsDialogFragment.newInstance(compteur);
        editNameDialogFragment.show(fragmentManager, "fragment_ensoi_stats");
    }

    /**
     * A l'interruption de l'activité
     */
    @Override
    protected void onStop() {
        ((MyApplication)getApplication()).closeDatabase();
        super.onStop();
    }
}