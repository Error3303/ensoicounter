package com.error3303.ensoicounter.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.error3303.ensoicounter.entity.EnSoi;

import java.util.List;

@Dao
public interface EnSoiDao {

    /**
     * Permet la récupération de l'ensemble des occurences de la table ensoi.
     * @return un LiveData contenant une liste de EnSoi.
     */
    @Query("SELECT * FROM ensoi")
    LiveData<List<EnSoi>> getEnsois();

    /**
     * Permet l'insertion d'un EnSoi dans la bdd.
     * @param ensoi l'EnSoi à insérer.
     */
    @Insert
    void insertEnSoi(EnSoi ensoi);

    /**
     * Permet la suppression du dernier EnSoi en bdd.
     */
    @Query("DELETE FROM ensoi WHERE id=(SELECT max(id) FROM ensoi)")
    void deleteLastEnSoi();

    /**
     * Permet la suppression de l'ensemble des EnSoi de la bdd.
     */
    @Query("DELETE FROM ensoi")
    void deleteAll();

    /**
     * Retourne la moyenne du nb de "En soi" par jour.
     * @return un entier
     */
    @Query("SELECT avg(nb_ensoi) FROM (SELECT count(date) as nb_ensoi FROM ensoi GROUP BY date)")
    int getAverage();

    /**
     * Retourne la liste des dates distinctes.
     * @return une liste de String
     */
    @Query("SELECT DISTINCT date FROM ensoi")
    List<String> getDistinctDates();

    /**
     * Retourne le nombre de "En soi" pour une date.
     * @param date
     * @return un entier
     */
    @Query("SELECT count(id) FROM ensoi WHERE date = :date")
    int getNbDate(String date);

    /**
     * Retourne le nombre min de "En soi".
     * @return un entier
     */
    @Query("SELECT min(nb_ensoi) FROM (SELECT count(date) as nb_ensoi FROM ensoi GROUP BY date)")
    int getMinNb();

    /**
     * Retourne le nombre max de "En soi".
     * @return un entier
     */
    @Query("SELECT max(nb_ensoi) FROM (SELECT count(date) as nb_ensoi FROM ensoi GROUP BY date)")
    int getMaxNb();
}
