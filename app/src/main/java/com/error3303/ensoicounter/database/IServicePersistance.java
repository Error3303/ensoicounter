package com.error3303.ensoicounter.database;

import androidx.lifecycle.LiveData;

import com.error3303.ensoicounter.entity.EnSoi;

import java.util.List;

public interface IServicePersistance {

    LiveData<List<EnSoi>> getEnsois();

    void insert(EnSoi ensoi);

    void deleteLastEnSoi();

    void deleteAll();

    int getEnSoiAverage();

    List<String> getDistinctDates();

    int getNbEnSoiDate(String date);

    int getEnSoiMin();

    int getEnSoiMax();

}
