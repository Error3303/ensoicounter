package com.error3303.ensoicounter.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.error3303.ensoicounter.entity.EnSoi;


@Database(entities = {EnSoi.class}, version = 1, exportSchema = false)
public abstract class EnSoiDatabase extends RoomDatabase {

    // --- DAO ---
    public abstract EnSoiDao enSoiDao();

    /**
     * Retourne  une instance de EnSoiDatabase
     * @param context
     * @return EnSoiDatabase
     */
    public static EnSoiDatabase getInstance(Context context) {
        synchronized (EnSoi.class) {
            return Room.databaseBuilder(context,
                    EnSoiDatabase.class, "database.db")
                    .allowMainThreadQueries()
                    .build();
        }
    }
}
