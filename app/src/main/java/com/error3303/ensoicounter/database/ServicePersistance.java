package com.error3303.ensoicounter.database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.error3303.ensoicounter.MyApplication;
import com.error3303.ensoicounter.entity.EnSoi;

import java.util.List;

public class ServicePersistance implements IServicePersistance {

    private EnSoiDao dao;

    public ServicePersistance(Application application){
        dao = ((MyApplication) application).getDao();
    }

    /**
     * Permet la récupération de l'ensemble des occurences de la table ensoi.
     * @return un LiveData contenant une liste de EnSoi.
     */
    @Override
    public LiveData<List<EnSoi>> getEnsois() {
        return dao.getEnsois();
    }

    /**
     * Permet l'insertion d'un EnSoi dans la bdd.
     * @param ensoi l'EnSoi à insérer.
     */
    @Override
    public void insert(EnSoi ensoi) {
        dao.insertEnSoi(ensoi);
    }

    /**
     * Permet la suppression du dernier EnSoi en bdd.
     */
    @Override
    public void deleteLastEnSoi() {
        dao.deleteLastEnSoi();
    }

    /**
     * Permet la suppression de l'ensemble des EnSoi de la bdd.
     */
    @Override
    public void deleteAll() {
        dao.deleteAll();
    }

    /**
     * Retourne la moyenne du nb de "En soi" par jour.
     * @return un entier
     */
    @Override
    public int getEnSoiAverage() {
        return dao.getAverage();
    }

    /**
     * Retourne la liste des dates distinctes.
     * @return une liste de String
     */
    @Override
    public List<String> getDistinctDates() {
        return dao.getDistinctDates();
    }

    /**
     * Retourne le nombre de "En soi" pour une date.
     * @param date
     * @return un entier
     */
    @Override
    public int getNbEnSoiDate(String date) {
        return dao.getNbDate(date);
    }

    /**
     * Retourne le nombre min de "En soi".
     * @return un entier
     */
    @Override
    public int getEnSoiMin() {
        return dao.getMinNb();
    }

    /**
     * Retourne le nombre max de "En soi".
     * @return un entier
     */
    @Override
    public int getEnSoiMax() {
        return dao.getMaxNb();
    }
}
