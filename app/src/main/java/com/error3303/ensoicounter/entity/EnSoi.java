package com.error3303.ensoicounter.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Entity(tableName = "ensoi")
@Getter
@Setter
public class EnSoi {

    @PrimaryKey(autoGenerate = true)
    protected int id;

    private String date;

    private String heure;

    private String mot;

    /**
     * Constructeur
     */
    public EnSoi(){
        Date newDate = new Date();
        date = dateFormat(newDate);
        heure = hourFormat(newDate);
        mot = "en soi";
    }

    /**
     * Formate une date en Jour/Mois/Annee
     * @param date
     * @return une chaine de caracteres au format "dd/MM/yy"
     */
    private String dateFormat(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        return dateFormat.format(date);
    }

    /**
     * Formate une date en Heure:minute
     * @param date
     * @return une chaine de caracteres au format "HH:mm"
     */
    private String hourFormat(Date date){
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
        return hourFormat.format(date);
    }
}
