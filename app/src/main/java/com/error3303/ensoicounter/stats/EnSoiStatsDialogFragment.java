package com.error3303.ensoicounter.stats;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.error3303.ensoicounter.R;
import com.error3303.ensoicounter.database.ServicePersistance;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EnSoiStatsDialogFragment extends DialogFragment {

    private ServicePersistance servicePersistance;

    @BindView(R.id.lbl_compteur_occurences_stats)
    TextView lblCompteurOccurences;
    @BindView(R.id.lbl_compteur_moyenne_stats)
    TextView lblCompteurMoyenne;
    @BindView(R.id.lbl_min_value)
    TextView lblMin;
    @BindView(R.id.lbl_max_value)
    TextView lblMax;
    @BindView(R.id.bar_chart)
    BarChart barChart;

    public EnSoiStatsDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    /**
     * Constructeur de EnSoiStatsDialogFragment
     *
     * @param compteur Le compteur actuel de "en soi"
     * @return
     */
    public static EnSoiStatsDialogFragment newInstance(int compteur) {
        EnSoiStatsDialogFragment fragment = new EnSoiStatsDialogFragment();
        Bundle args = new Bundle();
        args.putInt("compteur", compteur);
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * Création de la vue depuis le layout.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ensoi_stats, container);
    }

    /**
     * Se déclenche lorsque la vue est créée.
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        init();
    }

    /**
     * Initialise les elements
     */
    private void init() {
        servicePersistance = new ServicePersistance(getActivity().getApplication());

        getDialog().setTitle("Stats");
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        lblCompteurOccurences.setText(String.valueOf(getArguments().getInt("compteur", 0)));
        lblCompteurMoyenne.setText(String.valueOf(servicePersistance.getEnSoiAverage()));
        lblMin.setText(String.valueOf(servicePersistance.getEnSoiMin()));
        lblMax.setText(String.valueOf(servicePersistance.getEnSoiMax()));

        createChart();
    }

    /**
     * Retourne une map ayant pour clé une date et comme valeur un entier
     * representant le nombre de "en soi".
     *
     * @return Map<String, Integer>
     */
    private Map<String, Integer> getDatas() {
        Map<String, Integer> datas = new TreeMap<>();

        List<String> dates = servicePersistance.getDistinctDates();
        for (String date : dates) {
            int value = servicePersistance.getNbEnSoiDate(date);
            datas.put(date, value);
        }

        return datas;
    }

    /**
     * Retourne une liste d'elements d'entrée pour le diagramme.
     *
     * @param values Collection d'entier contenant les valeurs à ajouter au diagramme.
     * @return Liste de BarEntry<Integer, Integer> ayant pour clé un index
     * et comme valeur la valeur a ajouter au diagramme.
     */
    private ArrayList<BarEntry> dataToBarEntry(Collection<Integer> values) {
        ArrayList<BarEntry> data = new ArrayList<>();
        int index = 0;

        for (Integer value : values) {
            data.add(new BarEntry(index, value));
            index++;
        }

        return data;
    }

    /**
     * Créé et paramètre l'aspect graphique du diagramme (+ axe X et axe Y).
     * Appelle ensuite la methode setDatas().
     */
    private void createChart() {
        barChartOptions();

        XAxis xAxis = barChart.getXAxis();
        xAxisOptions(xAxis);

        YAxis yAxis = barChart.getAxisLeft();
        yAxisOptions(yAxis);

        setDatas(xAxis, getDatas());
    }

    /**
     * Attribue les données à chaque élément du diagramme (axe X, Y, données dans le diagramme).
     *
     * @param xAxis l'axe X affichant les dates.
     * @param datas une Map<String, Integer> ayant pour clé une date et
     *              comme valeur le nombre de en soi pour cette date.
     */
    private void setDatas(XAxis xAxis, Map<String, Integer> datas) {
        //Valeurs de l'abscisse (dates)
        xAxis.setValueFormatter(new IndexAxisValueFormatter(datas.keySet()));

        //Valeurs dans le diagramme (nombre de en soi)
        BarDataSet barDataSet = new BarDataSet(dataToBarEntry(datas.values()), "En soi");
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(barDataSet);

        BarData data = new BarData(dataSets);
        barChart.setData(data);
    }

    /**
     * Défini les paramètres du diagramme BarChart.
     */
    private void barChartOptions() {
        barChart.getDescription().setEnabled(false);
        barChart.setNoDataText("Pas de données disponible");
        barChart.setBackgroundColor(Color.WHITE);
        barChart.getAxisRight().setEnabled(false);
        barChart.setTouchEnabled(false);
        barChart.setDoubleTapToZoomEnabled(false);
    }

    /**
     * Défini les paramètres de l'abscisse.
     *
     * @param xAxis l'axe X
     */
    private void xAxisOptions(XAxis xAxis) {
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1);
        xAxis.setTextColor(Color.BLACK);
    }

    /**
     * Défini les paramètres de l'ordonnée.
     *
     * @param yAxis l'axe Y
     */
    private void yAxisOptions(YAxis yAxis) {
        yAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        yAxis.setAxisMinimum(0);
        yAxis.setAxisMaximum(50);
        yAxis.setYOffset(6f);
        yAxis.setTextColor(Color.BLACK);
    }
}
